use test;


DELETE FROM Relese;
DELETE FROM Room_Section;
DELETE FROM Nurse_Special_Room;
DELETE FROM Nurse_Public_Room;
DELETE FROM Visit;
DELETE FROM Hospitalize;
DELETE FROM Expriment;
DELETE FROM OrderMed;
DELETE FROM Treatment;
DELETE FROM Patient;
DELETE FROM Record;
DELETE FROM Cashier;
DELETE FROM Trainee;
DELETE FROM OnCall;
DELETE FROM Perment;
DELETE FROM Doctor;
DELETE FROM DrugMaker;
DELETE FROM Nurse;
DELETE FROM Receptionist;
DELETE FROM Employee;
DELETE FROM Public_Room;
DELETE FROM Special_Room;
DELETE FROM Room;
DELETE FROM Section;
DELETE FROM Medicine;

DECLARE @FromDate DATETIME
DECLARE @ToDate   DATETIME

SET @FromDate = '2010-01-01 08:22:13' 
SET @ToDate = '2019-03-05 17:56:31'
DECLARE @Seconds INT = DATEDIFF(SECOND, @FromDate, @ToDate)

Declare @CountSection int
Declare @CountPublic_Room int
Declare @CountSpecial_Room int
Declare @CountPatient int
Declare @CountDoctor int
Declare @CountNurse int
Declare @CountReceptionist int
Declare @CountDrugMaker int
Declare @CountCashier int
Declare @CountRecord int
Declare @CountTreatment int
Declare @CountMedicine int

Set @CountSection = 5
Set @CountPublic_Room = 90
Set @CountSpecial_Room = 10
Set @CountPatient = 1000
Set @CountDoctor = 30
Set @CountNurse = 180
Set @CountReceptionist = 5
Set @CountDrugMaker = 3
Set @CountCashier = 2
Set @CountRecord = 1000
Set @CountTreatment = 4000
Set @CountMedicine = 4000

Declare @Id int
Declare @sex nvarchar(10)

Set @Id = 1
while @Id <= @CountSection begin
	Insert Into Section (id, name) values (
		@id,
		'section - ' + CAST(@Id as nvarchar(10))
	)
	Set @Id = @Id + 1
end

Set @Id = 1
while @Id <= @CountPublic_Room begin
	Insert Into Room(id) values (@id)
	Insert Into Public_Room(id,rid) values (
		@id,
		@id
	)
	Set @Id = @Id + 1
end

while @Id <= @CountSpecial_Room + @CountPublic_Room begin
	Insert Into Room(id) values (@id)
	Insert Into Special_Room(id, rid) values (
		@id - @CountPublic_Room,
		@id
	)
	Set @Id = @Id + 1
end

Set @Id = 1
While @Id <= @CountCashier Begin 
	Set @sex = FLOOR(RAND()*(1-0+1));
	if @sex = 1 BEGIN
		Set @sex = 'male'
	END ELSE BEGIN
		Set @sex = 'female'
	END
	Insert Into Employee (id, name, sallary, sex, address, history, birthYear) values (
		@id,
		'employee - ' + CAST(@Id as nvarchar(10)),
		FLOOR(RAND()*(500-1+1) + 1) * 10000 + 1000000,
		@sex,
		'address - ' + 'address' + CAST(@Id as nvarchar(10)),
		'history - ' + CAST(@Id as nvarchar(10)),
		@id % 30 + 20
	)
	Insert Into Cashier(id,eid) values (
		@id,
		@id
	)
	Set @Id = @Id + 1
End

While @Id <= @CountCashier + @CountDoctor Begin 
	Set @sex = FLOOR(RAND()*(1-0+1));
	if @sex = 1 BEGIN
		Set @sex = 'male'
	END ELSE BEGIN
		Set @sex = 'female'
	END
	Insert Into Employee (id, name, sallary, sex, address, history, birthYear) values (
		@id,
		'employee - ' + CAST(@Id as nvarchar(10)),
		10000 + @id,
		@sex,
		'address - ' + 'address' + CAST(@Id as nvarchar(10)),
		'history - ' + CAST(@Id as nvarchar(10)),
		@id % 30 + 20
	)
	Insert Into Doctor(id,eid, special) values (
		@id - @CountCashier,
		@id,
		'special - ' + CAST(@Id as nvarchar(10))
	)
	if @id % 3 = 0 begin
		Insert Into Trainee(id, did) values (
			@id,
			@id - @CountCashier
		)
	end else if @id % 3 = 1 begin
		Insert Into OnCall(id,did) values (
			@id,
			@id - @CountCashier
		)
	end else if @id % 3 = 2 begin
		Insert Into Perment(id,did) values (
			@id,
			@id - @CountCashier
		)
	end
	Set @Id = @Id + 1
End

While @Id <= @CountCashier + @CountDoctor + @CountDrugMaker Begin 
	Set @sex = FLOOR(RAND()*(1-0+1));
	if @sex = 1 BEGIN
		Set @sex = 'male'
	END ELSE BEGIN
		Set @sex = 'female'
	END
	Insert Into Employee (id, name, sallary, sex, address, history, birthYear) values (
		@id,
		'employee - ' + CAST(@Id as nvarchar(10)),
		10000 + @id,
		@sex,
		'address - ' + 'address' + CAST(@Id as nvarchar(10)),
		'history - ' + CAST(@Id as nvarchar(10)),
		@id % 30 + 20
	)
	Insert Into DrugMaker(eid, id) values (
		@id - @CountCashier - @CountDoctor,
		@id
	)
	Set @Id = @Id + 1
End

While @Id <= @CountCashier + @CountDoctor + @CountDrugMaker + @CountNurse Begin 
	Set @sex = FLOOR(RAND()*(1-0+1));
	if @sex = 1 BEGIN
		Set @sex = 'male'
	END ELSE BEGIN
		Set @sex = 'female'
	END
	Insert Into Employee (id, name, sallary, sex, address, history, birthYear) values (
		@id,
		'employee - ' + CAST(@Id as nvarchar(10)),
		10000 + @id,
		@sex,
		'address - ' + 'address' + CAST(@Id as nvarchar(10)),
		'history - ' + CAST(@Id as nvarchar(10)),
		@id % 30 + 20
	)
	Insert Into Nurse(eid,id) values (
		@id,
		@id - @CountCashier - @CountDoctor - @CountDrugMaker
	)
	Set @Id = @Id + 1
End

While @Id <= @CountCashier + @CountDoctor + @CountDrugMaker + @CountNurse + @CountReceptionist Begin 
	Set @sex = FLOOR(RAND()*(1-0+1));
	if @sex = 1 BEGIN
		Set @sex = 'male'
	END ELSE BEGIN
		Set @sex = 'female'
	END
	Insert Into Employee (id, name, sallary, sex, address, history, birthYear) values (
		@id,
		'employee - ' + CAST(@Id as nvarchar(10)),
		10000 + @id,
		@sex,
		'address - ' + 'address' + CAST(@Id as nvarchar(10)),
		'history - ' + CAST(@Id as nvarchar(10)),
		@id % 30 + 20
	)
	Insert Into Receptionist(eid,id) values (
		@id,
		@id - @CountCashier - @CountDoctor - @CountDrugMaker - @CountNurse
	)
	Set @Id = @Id + 1
End

Set @Id = 1
while @Id <= @CountRecord begin
	
	Insert Into Record(id,distination,duration,rid,pid) values (
		@id,
		'distination - ' + CAST(@Id as nvarchar(10)),
		'duration - ' + CAST(@Id as nvarchar(10)),
		FLOOR(RAND()*(@CountReceptionist-1) + 1),
		FLOOR(RAND()*(@CountPatient + 1-1) + 1)
	)
	Set @Id = @Id + 1
end

Set @Id = 1
while @Id <= @CountPatient begin
	Set @sex = FLOOR(RAND()*(1-0+1));
	if @sex = 1 BEGIN
		Set @sex = 'male'
	END ELSE BEGIN
		Set @sex = 'female'
	END
	Insert Into Patient(id,address,details,name,sex,birthDay) values (
		@id,
		'address - ' + CAST(@Id as nvarchar(10)),
		'details - ' + CAST(@Id as nvarchar(10)),
		'name-' + CAST(@Id as nvarchar(10)),
		@sex,
		@id % 80
	)
	Set @Id = @Id + 1
end

Set @Id = 1
while @Id <= @CountPublic_Room begin
	Insert Into Nurse_Public_Room(pid,nid) values (
		@id,
		FLOOR(RAND()*(@CountNurse-1+1) + 1)
	)
	Set @Id = @Id + 1
end

Set @Id = 1
while @Id <= @CountSpecial_Room begin
	Insert Into Nurse_Special_Room(sid,nid) values (
		@id,
		@id
	)
	Set @Id = @Id + 1
end

Set @Id = 1
while @Id <= @CountTreatment begin
	Insert Into Treatment(id,cid,price) values (
		@id,
		FLOOR(RAND()*(@CountCashier-1+1) + 1),
		FLOOR(RAND()*(200-1+1) + 1) * 1000
	)
	if @id % 4 = 1 begin
		Insert Into Visit(id,tid,did,pid,rid,visistime) values (
			@id,
			@id,
			FLOOR(RAND()*(@CountDoctor-1+1) + 1),
			FLOOR(RAND()*(@CountPatient-1+1) + 1),
			FLOOR(RAND()*(@CountReceptionist-1+1) + 1),
			DATEADD(SECOND, ROUND(((@Seconds-1) * RAND()), 0), @FromDate)
		)
	end else if @id % 4 = 2 begin
		Insert Into Expriment(id,tid,did,pid) values (
			@id,
			@id,
			FLOOR(RAND()*(@CountDoctor-1+1) + 1),
			FLOOR(RAND()*(@CountPatient-1+1) + 1)
		)
	end else if @id % 4 = 3 begin
		Insert Into OrderMed(id,tid,description,pid,did) values (
			@id,
			@id,
			'description - ' + CAST(@Id as nvarchar(10)),
			FLOOR(RAND()*(@CountPatient-1+1) + 1),
			FLOOR(RAND()*(@CountDoctor-1+1) + 1)
		)
	end else if @id % 4 = 0 begin
		Insert Into Hospitalize(id,tid,endDate,pid,reid,roid,startDate) values (
			@id,
			@id,
			DATEADD(SECOND, ROUND(((@Seconds-1) * RAND()), 0), @FromDate),
			FLOOR(RAND()*(@CountPatient-1+1) + 1),
			FLOOR(RAND()*(@CountReceptionist-1+1) + 1),
			FLOOR(RAND()*(@CountPublic_Room + @CountSpecial_Room-1+1) + 1),
			DATEADD(SECOND, ROUND(((@Seconds-1) * RAND()), 0), @FromDate)
		)
	end
	Set @Id = @Id + 1
end

Set @Id = 1
while @Id <= @CountSpecial_Room + @CountPublic_Room begin
	Insert Into Room_Section(rid, sid) values (@Id, FLOOR(RAND()*(@CountSection-1+1) + 1))
	Set @Id = @Id + 1
end

Set @Id = 1
while @Id <= @CountPatient begin
	Insert Into Relese(id,pid,did,rid,statusRelease) values (
		@id,
		@id,
		FLOOR(RAND()*(@CountDoctor-1+1) + 1),
		FLOOR(RAND()*(@CountReceptionist-1+1) + 1),
		'doctor'
	)
	Set @Id = @Id + 1
end

Set @Id = 1
while @Id <= @CountMedicine begin
	Insert Into Medicine(id,company,expDate,number) values (
		@id,
		'company-' + CAST(@Id as nvarchar(10)),
		DATEADD(SECOND, ROUND(((@Seconds-1) * RAND()), 0), @FromDate),
		FLOOR(RAND()*(300 + 1-1) + 1)
	)
	Set @Id = @Id + 1
end