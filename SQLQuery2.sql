DROP DATABASE IF EXISTS test
GO

CREATE DATABASE test
GO

USE test
GO

CREATE TABLE Employee(
	id bigint NOT NULL,
	name char(100) NOT NULL,
	sallary bigint NULL,
	sex char(10) NULL,
	address char(400) NULL,
	history char(400) NULL,
	birthYear int,
	CONSTRAINT PK_Employee PRIMARY KEY (id)
)
GO

CREATE TABLE Section(
	id bigint NOT NULL,
	name char(100) NOT NULL,
	CONSTRAINT PK_Section PRIMARY KEY (id)
)
GO

CREATE TABLE Employee_Section(
	sid bigint NOT NULL ,
	eid bigint  NOT NULL,
	CONSTRAINT PK_Employee_Section PRIMARY KEY (eid),
	CONSTRAINT FK_Employee_Section_To_Section FOREIGN KEY(sid) REFERENCES Section(id),
	CONSTRAINT FK_Employee_Section_To_Employee FOREIGN KEY(eid) REFERENCES Employee(id)
)
GO

CREATE TABLE DrugMaker(
	id bigint NOT NULL,
	eid bigint  NOT NULL,
	CONSTRAINT PK_DrugMaker PRIMARY KEY (id),
	CONSTRAINT FK_DrugMaker_To_Employee FOREIGN KEY(eid) REFERENCES Employee(id)
)
GO

CREATE TABLE Cashier(
	id bigint NOT NULL,
	eid bigint  NOT NULL,
	CONSTRAINT PK_Cashier PRIMARY KEY (id),
	CONSTRAINT FK_Cashier_To_Employee FOREIGN KEY(eid) REFERENCES Employee(id)
)
GO

CREATE TABLE Nurse(
	id bigint NOT NULL,
	eid bigint  NOT NULL,
	CONSTRAINT PK_Nurse PRIMARY KEY (id),
	CONSTRAINT FK_Nurse_To_Employee FOREIGN KEY(eid) REFERENCES Employee(id)
)
GO

CREATE TABLE Receptionist(
	id bigint NOT NULL,
	eid bigint  NOT NULL,
	CONSTRAINT PK_Receptionist PRIMARY KEY (id),
	CONSTRAINT FK_Receptionist_To_Employee FOREIGN KEY(eid) REFERENCES Employee(id)
)
GO

CREATE TABLE Doctor(
	id bigint NOT NULL,
	eid bigint  NOT NULL,
	special char(100) NOT NULL,
	CONSTRAINT PK_Doctor PRIMARY KEY (id),
	CONSTRAINT FK_Doctor_To_Employee FOREIGN KEY(eid) REFERENCES Employee(id)
)
GO

CREATE TABLE Perment(
	id bigint NOT NULL,
	did bigint  NOT NULL,
	CONSTRAINT PK_Perment PRIMARY KEY (id),
	CONSTRAINT FK_Perment_To_Doctor FOREIGN KEY(did) REFERENCES Doctor(id)
)
GO

CREATE TABLE Trainee(
	id bigint NOT NULL,
	did bigint  NOT NULL,
	CONSTRAINT PK_Trainee PRIMARY KEY (id),
	CONSTRAINT FK_Trainee_To_Doctor FOREIGN KEY(did) REFERENCES Doctor(id)
)
GO

CREATE TABLE OnCall(
	id bigint NOT NULL,
	did bigint  NOT NULL,
	CONSTRAINT PK_OnCall PRIMARY KEY (id),
	CONSTRAINT FK_OnCall_To_Doctor FOREIGN KEY(did) REFERENCES Doctor(id)
)
GO

CREATE TABLE Doctor_Doctor(
	did1 bigint NOT NULL,
	did2 bigint  NOT NULL,
	CONSTRAINT PK_Doctor_Doctor PRIMARY KEY (did1, did2),
	CONSTRAINT FK_Doctor_Doctor1_To_Doctor FOREIGN KEY(did1) REFERENCES Doctor(id),
	CONSTRAINT FK_Doctor_Doctor2_To_Doctor FOREIGN KEY(did2) REFERENCES Doctor(id)
)
GO

CREATE TABLE Room(
	id bigint NOT NULL,
	CONSTRAINT PK_Room PRIMARY KEY (id),
)
GO

CREATE TABLE Room_Section(
	rid bigint NOT NULL,
	sid bigint NOT NULL,
	CONSTRAINT PK_Room_Section PRIMARY KEY (rid),
	CONSTRAINT FK_Room_Section_To_Room FOREIGN KEY(rid) REFERENCES Room(id),
	CONSTRAINT FK_Room_Section_To_Section FOREIGN KEY(sid) REFERENCES Section(id)
)
GO

CREATE TABLE Public_Room(
	id bigint NOT NULL,
	rid bigint  NOT NULL,
	CONSTRAINT PK_Public_Room PRIMARY KEY (id),
	CONSTRAINT FK_Public_Room_To_Room FOREIGN KEY(rid) REFERENCES Room(id)
)
GO

CREATE TABLE Special_Room(
	id bigint NOT NULL,
	rid bigint  NOT NULL,
	CONSTRAINT PK_Special_Room PRIMARY KEY (id),
	CONSTRAINT FK_Special_Room_To_Room FOREIGN KEY(rid) REFERENCES Room(id)
)
GO

CREATE TABLE Nurse_Special_Room(
	nid bigint NOT NULL,
	sid bigint  NOT NULL,
	CONSTRAINT PK_Nurse_Special_Room PRIMARY KEY (nid),
	CONSTRAINT FK_Nurse_Special_Room_To_Special_Room FOREIGN KEY(nid) REFERENCES Nurse(id),
	CONSTRAINT FK_Nurse_Special_Room_To_Nurse FOREIGN KEY(sid) REFERENCES Special_Room(id)
)
GO

CREATE TABLE Nurse_Public_Room(
	nid bigint NOT NULL,
	pid bigint  NOT NULL,
	CONSTRAINT PK_Nurse_Public_Room PRIMARY KEY (nid, pid),
	CONSTRAINT FK_Nurse_Public_Room_To_Public_Room FOREIGN KEY(nid) REFERENCES Nurse(id),
	CONSTRAINT FK_Nurse_Public_Room_To_Nurse FOREIGN KEY(pid) REFERENCES Public_Room(id)
)
GO

CREATE TABLE Record(
	id bigint NOT NULL,
	pid bigint NOT NULL,
	rid bigint NOT NULL,
	duration char(100) NULL,
	distination char(100) NULL,
	CONSTRAINT PK_Record PRIMARY KEY (id),
	CONSTRAINT FK_Record_To_Receptionist FOREIGN KEY(rid) REFERENCES Receptionist(id),
)
GO

CREATE TABLE Patient(
	id bigint NOT NULL,
	name char(100) NOT NULL,
	details char(100) NULL,
	sex char(10) NULL,
	address char(400) NULL,
	birthDay int,
	CONSTRAINT PK_Patient PRIMARY KEY (id)
)
GO

--CREATE INDEX IX_Patient_Name ON Patient(birthDay);

CREATE TABLE Treatment(
	id bigint NOT NULL,
	price bigint NULL,
	cid bigint NULL,
	CONSTRAINT PK_Treatment PRIMARY KEY (id),
	CONSTRAINT FK_Treatment_To_Cashier FOREIGN KEY(cid) REFERENCES Cashier(id)
)
GO

CREATE TABLE Hospitalize(
	id bigint NOT NULL,
	tid bigint NOT NULL,
	roid bigint NOT NULL,
	reid bigint NOT NULL,
	pid bigint NOT NULL,
	startDate datetime NULL,
	endDate datetime NULL,
	CONSTRAINT PK_Hospitalize PRIMARY KEY (id),
	CONSTRAINT FK_Hospitalize_To_Treatment FOREIGN KEY(tid) REFERENCES Treatment(id),
	CONSTRAINT FK_Hospitalize_To_Room FOREIGN KEY(roid) REFERENCES Room(id),
	CONSTRAINT FK_Hospitalize_To_Receptionist FOREIGN KEY(reid) REFERENCES Receptionist(id),
	CONSTRAINT FK_Hospitalize_To_Patient FOREIGN KEY(pid) REFERENCES Patient(id)
)
GO

CREATE TABLE Expriment(
	id bigint NOT NULL,
	tid bigint NOT NULL,
	pid bigint NOT NULL,
	did bigint NOT NULL,
	CONSTRAINT PK_Expriment PRIMARY KEY (id),
	CONSTRAINT FK_Expriment_To_Treatment FOREIGN KEY(tid) REFERENCES Treatment(id),
	CONSTRAINT FK_Expriment_To_Patient FOREIGN KEY(pid) REFERENCES Patient(id),
	CONSTRAINT FK_Expriment_To_Doctor FOREIGN KEY(did) REFERENCES Doctor(id)
)
GO

CREATE TABLE OrderMed(
	id bigint NOT NULL,
	tid bigint NOT NULL,
	pid bigint NOT NULL,
	did bigint NOT NULL,
	description char(1000) NULL,
	CONSTRAINT PK_OrderMed PRIMARY KEY (id),
	CONSTRAINT FK_OrderMed_To_Treatment FOREIGN KEY(tid) REFERENCES Treatment(id),
	CONSTRAINT FK_OrderMed_To_Patient FOREIGN KEY(pid) REFERENCES Patient(id),
	CONSTRAINT FK_OrderMed_To_Doctor FOREIGN KEY(did) REFERENCES Doctor(id)
)
GO

CREATE TABLE Visit(
	id bigint NOT NULL,
	tid bigint NOT NULL,
	pid bigint NOT NULL,
	rid bigint NOT NULL,
	did bigint NOT NULL,
	visistime datetime NULL,
	CONSTRAINT PK_Visit PRIMARY KEY (id),
	CONSTRAINT FK_Visit_To_Treatment FOREIGN KEY(tid) REFERENCES Treatment(id),
	CONSTRAINT FK_Visit_To_Patient FOREIGN KEY(pid) REFERENCES Patient(id),
	CONSTRAINT FK_Visit_To_Receptionist FOREIGN KEY(rid) REFERENCES Receptionist(id),
	CONSTRAINT FK_Visit_To_Doctor FOREIGN KEY(did) REFERENCES Doctor(id)
)
GO

CREATE TABLE Medicine(
	id bigint NOT NULL,
	name char(100) NULL,
	expDate date NULL,
	number bigint NOT NULL,
	company char(100) NULL,
	CONSTRAINT PK_Medicine PRIMARY KEY (id)
)
GO



CREATE TABLE Relese(
	id bigint NOT NULL,
	did bigint NOT NULL,
	pid bigint NOT NULL,
	rid bigint NOT NULL,
	statusRelease varchar(10),
	CONSTRAINT PK_Relese PRIMARY KEY (id),
	CONSTRAINT FK_Relese_To_Patient FOREIGN KEY(pid) REFERENCES Patient(id),
	CONSTRAINT FK_Relese_To_Receptionist FOREIGN KEY(rid) REFERENCES Receptionist(id),
	CONSTRAINT FK_Relese_To_Doctor FOREIGN KEY(did) REFERENCES Doctor(id)
)
GO

CREATE TABLE Patient_History
(
ID INT IDENTITY,
code Int,
name NVARCHAR(50),
ActionType  NVARCHAR(30),
ActionDate DATETIME,
PRIMARY KEY NONCLUSTERED (ID)
)

CREATE TABLE DDL_Log 
(
PostTime datetime, 
DB_User nvarchar(100), 
[Event] nvarchar(100), 
[TSQL] nvarchar(2000)
)
GO




