﻿use test;

SELECT COUNT(*) AS count FROM Employee;

SELECT AVG(birthYear) AS birthYearAvg FROM EMPLOYEE;

SELECT MAX(birthYear) AS birthYearMax FROM EMPLOYEE;

SELECT MIN(birthYear) AS birthYearMin FROM EMPLOYEE;

SELECT SUM(sallary) AS allSalleryInAYear FROM EMPLOYEE;

SELECT COUNT(*), birthYear FROM EMPLOYEE GROUP BY birthYear;

SELECT name, 
       sallary, 
       birthYear, 
       ROW_NUMBER() OVER(ORDER BY birthYear DESC) AS RowNumber
FROM EMPLOYEE;

SELECT name, 
       sallary, 
       birthYear, 
       ROW_NUMBER() OVER(PARTITION BY birthYear ORDER BY sallary DESC) AS RowNumber
FROM EMPLOYEE;

SELECT name, 
       sallary, 
       birthYear, 
       RANK() OVER(PARTITION BY birthYear ORDER BY sallary DESC) AS RowNumber
FROM EMPLOYEE;
-----***
SELECT name, 
       sallary, 
       birthYear, 
       DENSE_RANK() OVER(PARTITION BY birthYear ORDER BY sallary DESC) AS RowNumber
FROM EMPLOYEE;
-----***
--کارکنان به دو دسته بر حسب سن تقسیم می شوند
SELECT *,NTILE(2) OVER(ORDER BY birthYear DESC) Rank FROM EMPLOYEE;
--سه داروی گرونی که هر دکتر تجویز کرده است
WITH OrderMedRanks AS
(
  SELECT e.name as ename, t.price, m.description as mname, ROW_NUMBER() OVER(PARTITION BY e.name ORDER BY t.price DESC) AS Ranks
  FROM (((Doctor d inner join OrderMed m ON d.id = m.did) inner join Treatment t ON t.id = m.tid) inner join Employee e ON e.id = d.eid)
)
SELECT me.ename , me.price, me.mname
FROM OrderMedRanks me
WHERE Ranks >= 1 and Ranks <=3
ORDER BY me.ename
--آخرین شخص بستری شده در هر اتاق
SELECT * FROM 
(
SELECT 
ROW_NUMBER() OVER (PARTITION BY r.id ORDER BY h.startDate DESC) AS ROW_NO
,p.name,h.startDate,r.id
FROM ((Room r inner join Hospitalize h ON r.id = h.roid) inner join Patient p ON p.id = h.pid)
) Q
WHERE Q.ROW_NO=1
--متوسط حقوق دریافتی برحسب سن
SELECT birthYear, AVG(sallary) FROM Employee GROUP BY birthYear
-- نام اشخاص و متوسط حقوق دریافتی برحسب سن
SELECT name, birthYear, avg(sallary) OVER(PARTITION BY birthYear) FROM Employee 
-- مشخصات بیشترین فاکتور وحساب کننده ی فاکتور 
SELECT e.name,t2.price FROM Employee AS e CROSS APPLY (
	SELECT top 1 t.price FROM Treatment AS t WHERE t.cid = e.id ORDER BY t.price DESC
) AS t2
-----------------------------------------------------
--Insert ایجاد تریگر برای حالت 
CREATE TRIGGER TRIG_PAT_Insert ON Patient
AFTER INSERT
AS
INSERT INTO Patient_History(code,name,ActionType,ActionDate) 
SELECT id,name,'Insert',GETDATE() FROM inserted
GO
--Update ایجاد تریگر برای حالت 
CREATE TRIGGER trg_Patient_Update ON Patient
AFTER UPDATE
AS
--مقدار قبل از بروز رسانی
INSERT INTO Patient_History(code,name,ActionType,ActionDate) 
SELECT id,name,'Update-Old',GETDATE() FROM deleted
--مقدار پس از بروز رسانی
INSERT INTO Patient_History(code,name,ActionType,ActionDate) 
SELECT id,name,'Update-New',GETDATE() FROM inserted
GO
--Delete ایجاد تریگر برای حالت 
CREATE TRIGGER trg_Students_Delete ON Patient
AFTER DELETE
AS
INSERT INTO Students_History(code,name,ActionType,ActionDate) 
SELECT id,name,'Delete',GETDATE() FROM deleted
GO
--لاگ هر اتفاق
CREATE TRIGGER [DDL_Log] ON DATABASE 
FOR DDL_DATABASE_LEVEL_EVENTS 
AS
DECLARE @data XML
SET @data = EVENTDATA() 
INSERT ddl_log (PostTime, DB_User, [Event], [TSQL]) 
VALUES 
(
GETDATE(), 
CONVERT(nvarchar(100), CURRENT_USER), 
@data.value('(/EVENT_INSTANCE/EventType)[1]', 'nvarchar(100)'), 
@data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(2000)') 
) 
GO
-------------------------------------------------------------------------- check index
SELECT count(*) FROM Patient p INNER JOIN 
	   Visit v ON v.pid = p.id INNER JOIN
	   Doctor d ON d.id = v.did INNER JOIN
	   Employee e ON e.id = d.eid WHERE p.birthDay > 40 AND e.birthYear > 40 AND p.birthDay < 50
-------------------------------------------------------------------------- view
CREATE VIEW VD(name, birthYear,countVisit) 
AS 
SELECT 
	e.name,e.birthYear,COUNT(v.id)
FROM Visit v INNER JOIN Doctor d ON d.id = v.did INNER JOIN Employee e ON e.id = d.eid GROUP BY e.name, e.birthYear

SELECT * FROM VD
--------------------------------------------------------------------------- procedure
CREATE PROCEDURE loadPateintByName
				@name varchar(10)
AS
BEGIN 
	SELECT * FROM Patient p WHERE p.name = @name
END

EXEC loadPateintByName 'name-1'
---------------------------------------------------------------------------
CREATE PROCEDURE loadPateintByDateRelease
				@start datetime, @end datetime
AS
BEGIN 
	SELECT * FROM Patient p INNER JOIN Hospitalize h ON h.pid=p.id WHERE h.endDate > @start AND h.startDate < @end
END

EXEC loadPateintByDateRelease '2010-01-01 08:22:13' , '2019-01-01 08:22:13' 
---------------------------------------------------------------------------
CREATE PROCEDURE loadDoctorsRelated
AS
BEGIN 
	SELECT * FROM Doctor d1 INNER JOIN Doctor_Doctor dd ON dd.did1=d1.id INNER JOIN Doctor d2 ON d2.id=dd.did2
END

EXEC loadDoctorsRelated
--------------------------------------------------------------------------- function return farsi date
CREATE FUNCTION CalculatePersianDate( @intDate DATETIME )
RETURNS NVARCHAR(max)
BEGIN

DECLARE @shYear AS INT ,@shMonth AS INT ,@shDay AS INT ,@intYY AS INT ,@intMM AS INT ,@intDD AS INT ,@Kabiseh1 AS INT ,@Kabiseh2 AS INT ,@d1 AS INT ,@m1 AS INT, @shMaah AS NVARCHAR(max),@shRooz AS NVARCHAR(max),@DayCnt AS INT
DECLARE @DayDate AS NVARCHAR(max)

SET @intYY = DATEPART(yyyy, @intDate)

IF @intYY < 1000 SET @intYY = @intYY + 2000

SET @intMM = MONTH(@intDate)
SET @intDD = DAY(@intDate)
SET @shYear = @intYY - 622
SET @DayCnt = 5

IF ( ( @intYY - 1992 ) % 4 = 0) SET @Kabiseh1 = 0 ELSE SET @Kabiseh1 = 1

IF ( ( @shYear - 1371 ) % 4 = 0) SET @Kabiseh2 = 0 ELSE SET @Kabiseh2 = 1

SET @m1 = 1
SET @d1 = 1
SET @shMonth = 10
SET @shDay = 11

IF ( ( @intYY - 1993 ) % 4 = 0 ) SET @shDay = 12


WHILE ( @m1 != @intMM ) OR ( @d1 != @intDD )
BEGIN

  SET @d1 = @d1 + 1
  SET @DayCnt = @DayCnt + 1

  IF 
  (@d1 = 32 AND (@m1 = 1 OR @m1 = 3 OR @m1 = 5 OR @m1 = 7 OR @m1 = 8 OR @m1 = 10 OR @m1 = 12))
  OR
  (@d1 = 31 AND (@m1 = 4 OR @m1 = 6 OR @m1 = 9 OR @m1 = 11))
  OR
  (@d1 = 30 AND @m1 = 2 AND @Kabiseh1 = 1)
  OR
  (@d1 = 29 AND @m1 = 2 AND @Kabiseh1 = 0)
  BEGIN
    SET @m1 = @m1 + 1
    SET @d1 = 1
  END

  IF @m1 > 12
  BEGIN
    SET @intYY = @intYY + 1
    SET @m1 = 1
  END

  IF @DayCnt > 7 SET @DayCnt = 1

 SET @shDay = @shDay + 1

  IF
  (@shDay = 32 AND @shMonth < 7)
  OR
  (@shDay = 31 AND @shMonth > 6 AND @shMonth < 12)
  OR
  (@shDay = 31 AND @shMonth = 12 AND @Kabiseh2 = 1)
  OR
  (@shDay = 30 AND @shMonth = 12 AND @Kabiseh2 = 0)
  BEGIN
    SET @shMonth = @shMonth + 1
    SET @shDay = 1
  END

  IF @shMonth > 12
  BEGIN
    SET @shYear = @shYear + 1
    SET @shMonth = 1
  END

END

IF @shMonth=1 SET @shMaah=N'فروردین'
IF @shMonth=2 SET @shMaah=N'اردیبهشت'
IF @shMonth=3 SET @shMaah=N'خرداد'
IF @shMonth=4 SET @shMaah=N'تیر'
IF @shMonth=5 SET @shMaah=N'مرداد'
IF @shMonth=6 SET @shMaah=N'شهریور'
IF @shMonth=7 SET @shMaah=N'مهر'
IF @shMonth=8 SET @shMaah=N'آبان'
IF @shMonth=9 SET @shMaah=N'آذر'
IF @shMonth=10 SET @shMaah=N'دی'
IF @shMonth=11 SET @shMaah=N'بهمن'
IF @shMonth=12 SET @shMaah=N'اسفند'

IF @DayCnt=1 SET @shRooz=N'شنبه'
IF @DayCnt=2 SET @shRooz=N'یکشنبه'
IF @DayCnt=3 SET @shRooz=N'دوشنبه'
IF @DayCnt=4 SET @shRooz=N'سه‌شنبه'
IF @DayCnt=5 SET @shRooz=N'چهارشنبه'
IF @DayCnt=6 SET @shRooz=N'پنجشنبه'
IF @DayCnt=7 SET @shRooz=N'جمعه'

SET @DayDate = REPLACE(RIGHT(STR(@shYear, 4), 4), ' ', '0') + '/'+ REPLACE(STR(@shMonth, 2), ' ', '0') + '/' + REPLACE(( STR(@shDay,2) ), ' ', '0')
RETURN @DayDate
END

SELECT TOP 10 [dbo].CalculatePersianDate(v.visistime),e.name, p.name FROM Visit v INNER JOIN Doctor d ON d.id = v.did INNER JOIN Employee e ON e.id = d.eid INNER JOIN Patient p ON p.id = v.pid
